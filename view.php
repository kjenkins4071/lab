<?php
      include_once("Model.php");  
      
    class View {  
         public $model;   
      
         public function __construct()  
         {  
              $this->model = new Model();  
         }   
      
         public function render()  
         {  
              if((isset($_GET['action']))  && $_GET['action'] == "insert"){
				
				include 'layout.php';
                include 'form.php'; 


				}else {
				$recipes = $this->model->findAll();
				include 'layout.php';
				include 'list.php'; 
              }  
         }  
    }  
	?>