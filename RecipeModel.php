<?php
    class Recipe { 
		public $id;
        public $title;  
        public $ingredient0;  
		public $ingredient1; 
		public $ingredient2;  
        public $instructions;  
      
        public function __construct($id,$title, $ingredient0,$ingredient1,$ingredient2, $instructions)  
        {  
            $this->id = $id;  
            $this->title = $title;  
            $this->ingredient0 = $ingredient0; 
			$this->ingredient1 = $ingredient1;  
            $this->ingredient2 = $ingredient2;  
            $this->instructions = $instructions; 
        }  
    }  
	?>