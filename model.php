<?php
    include_once("RecipeModel.php");  
      
    class Model {  
	
        public function findAll()  
        {  
            
            return array(  
                "0"=> new Recipe("0", "Spaghetti", "Tomatoes", "Garlic", "Basil", "Make it good."),
                "1"=> new Recipe("1", "Pad Thai", "Rice noodles", "Tamarind", "Green onions", "Make it gooder."),
                "2"=> new Recipe("2", "White Bean Soup", "Navy beans", "Ham hock", "Broth", "Make it goodest."),
            );  
        }  
      
        public function getRecipe($id)  
        {  
           
            $rec = $this->findAll();  
            return $rec[$id];  
        }  
      
    }  