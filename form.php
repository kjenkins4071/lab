<?php
   if($_SERVER['REQUEST_METHOD']=='GET'){
				?>
        <div class="container">
        	
        	
         <form action="index.php?action=insert" method="post">
		  			<fieldset>
			    		<legend>Insert a recipe</legend> 
			 			
						<label>Title</label>
			    		<input class="input-xxlarge" type="text" placeholder="Spaghetti" name="title">
			    				    		
			    		
						
			    		<label>Ingredients</label>
						
			    		<div><input class="input-xlarge" type="text" placeholder="Ingredient" name="ingredient0"></div>
		    		<div><input class="input-xlarge" type="text" placeholder="Ingredient" name="ingredient1"></div>
			    		<div><input class="input-xlarge" type="text" placeholder="Ingredient" name="ingredient2"></div>
			    			    		
			    		<label>Instructions</label>
			    		<div><textarea name="instructions" class="input-block-level" rows="5"></textarea></div>		    		
			    		
			


        		<button type="submit" class="btn btn-primary">Submit</button>
		  			</fieldset>
				</form>
				<a href="index.php">Return to recipe list</a>
				</div>
				<?php
				}elseif($_SERVER['REQUEST_METHOD']=='POST'){
				?>
				
			    
        <div class="container">
        	
			
			<h2 class="thanks">Thank you for submitting  <?php echo htmlentities($_POST["title"]); ?></h2>
			
			<h3><b>Ingredients</b></h3>
			<ul>
			<li><?php echo htmlentities($_POST["ingredient0"]); ?></li> 
			<li><?php echo htmlentities($_POST["ingredient1"]); ?></li> 
			<li><?php echo htmlentities($_POST["ingredient2"]); ?></li>
			</ul>
			<h3 class= "instructions"><b> Instructions</b></h3>
        	     <p><?php echo htmlentities($_POST["instructions"]); ?>   </p>	        
		     <a href="index.php">Return to recipe lists</a> 
		</div><?php
        }else{}
        ?>
         <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="bootstrap.min.js"></script>
    </body>
</html>